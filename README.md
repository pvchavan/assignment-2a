# WordCountDriver.java
- args[0] -> Path of input csv files directory only
- args[1] -> Path of output directory, frequency of each word is saved in this directory

Driver of MapReduce job. contains main method

# WordCountMapper.java
Mapper class of word count mapreduce job.
split csv to individual word and collects each word frequency
output - Text, Iterator(IntWritable) (Array of ones).

# WordCountReducer.java
Reducer class of word count job
input - Text and iterator(IntWritable)
output - writes to file the word and size of iterator against each word.


