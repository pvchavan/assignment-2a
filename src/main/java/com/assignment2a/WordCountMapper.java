package com.assignment2a;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;

public class WordCountMapper extends MapReduceBase implements Mapper<LongWritable,
        Text, Text, IntWritable> {


    @Override
    public void map(LongWritable longWritable,
                    Text text,
                    OutputCollector<Text, IntWritable> outputCollector,
                    Reporter reporter) throws IOException {

        String line = text.toString();
        line = line.replaceAll("\"", "")
                .replaceAll(" - ", " ")
                .replaceAll("-", " ")
                .replaceAll(", ", " ")
                .replaceAll(",", " ");
        System.out.println("total words: " + line.split(" ").length);
        for (String word : line.split(" ")) {
            if (word.length() > 0) {
                outputCollector.collect(new Text(word), new IntWritable(1));
            }
        }
    }
}

